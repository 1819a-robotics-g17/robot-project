 file# Robot project

Maze Navigation

## Robot overview:

We are going to do/create a robot, which has to drive out of a maze, while using image proscessing. The camera looks at the maze and the robot from top (Camera is attached to laptop). The image processing and sending commands to Pi on GoPiGo is done on a laptop. In the maze there is only one exit, but there might be some several valid paths to the exit( + The maze is with black lines and white background on the floor).

## Schedule for Pair A:

### Algorithm

* The robot tracks the line on the right and moves- turns along it. If there is no line on the right, then the robot will drive little bit futher to come out from labyrinth.
	1. Speed
	2. Driving 
	3. Turning
	
### Video Feedback 

* How to identify right line from camera picture?
1. When the robot reaches the correct position on the map, we give a command to action ( for example, turn right etc).

#
## Schedule for Pair B:
07.11
Do all of the plan to fixe the deadline.

   1° 
        1°1 create the maze on some paper sheet with 3 way to go out
        1°2 the different way to get out need to have different length
        1°3 think about the size of the robot for do the maze


26.11
    2°
        2°1 use image processing and thresholded to print out the localisation of the wall in the maze.
        2°2 use blop dectector to show the coordonnate of the wall.


12.12
    3° 
        3°1 use different color to mark the robot
        3°2 use a process to find the robot inside the maze
        3°3 do a treshold who just show the robot position
        3°4 change the scale of image to keep a good fps
        3°5 use blop dectection to show the position of the robot.

### Algorithm
	1. Line detection
	2. Robot localization
Challenge - Find the shortest way to exit; Solution - Use the A* algorithm, which is in the lab10 (implementations of that have not been successful yet).


# Component list:	

Item      |  Quantity | We need this component from You 
---------:| :---------|:-----:
GoPiGo robot  |    	 1    | +
Camera     |      1    | + 
Raspberry Pi      |      1    | +
Lipo battery | 1 | +
Laptop | 1 | -
SD card | 1 | +
Labyrinth | 1 | + (Do we have to do it?, How big it is?)


