import numpy as np
import cv2
import time
from FieldDisplay import *
from Node import *


cap = cv2.VideoCapture(1)

cv2.namedWindow("Output")
cv2.namedWindow("Robot")

lB = 0
lG = 0
lR = 0
hB = 255
hG = 255
hR = 255

trackbar_value = 72

maze = [[1 for x in range(64)] for y in range(48)]

def updateValue_lB(value):
    global lB
    lB = value
    return
cv2.createTrackbar("lB", "Output", trackbar_value, 255, updateValue_lB)

def updateValue_lG(value):
    global lG
    lG = value
    return
cv2.createTrackbar("lG", "Output", trackbar_value, 255, updateValue_lG)

def updateValue_lR(value):
    global lR
    lR = value
    return
cv2.createTrackbar("lR", "Output", trackbar_value, 255, updateValue_lR)

def updateValue_hB(value):
    global hB
    hB = value
    return
cv2.createTrackbar("hB", "Output", trackbar_value, 255, updateValue_hB)

def updateValue_hG(value):
    global hG
    hG = value
    return
cv2.createTrackbar("hG", "Output", trackbar_value, 255, updateValue_hG)

def updateValue_hR(value):
    global hR
    hR = value
    return

def get_line_location(frame):

	

	

	array2 = np.nonzero(thresholded)
	location = np.mean(array2[1])
	print(location)

def getnewlist():
    global newlist
    return newlist


cv2.createTrackbar("hR", "Output", trackbar_value, 255, updateValue_hR)

detector = cv2.SimpleBlobDetector_create()
blobparams = cv2.SimpleBlobDetector_Params()
blobparams.filterByCircularity = False
blobparams.filterByConvexity = False
blobparams.filterByInertia = False
blobparams.filterByArea = False
blobparams.maxArea = 200000000
blobparams.filterByColor = True
blobparams.blobColor = 255

blobparams.minArea = 500
detector = cv2.SimpleBlobDetector_create(blobparams)

t2 = time.time()
t1 = time.time()

while True:
    second= t2-t1
    
    fps= 1 / second
    
    
    t1 = time.time()
    
    ret, frame = cap.read()
    
    
    
    

    #cv2.putText(frame, str(int(fps)), (5, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

    
    
   # cv2.imshow("hello", frame)

    
    kernel = np.ones((5,5),np.float32)/5
   
    
    #frame = cv2.GaussianBlur(frame,(5,5),0)
    
    #frame = cv2.resize(frame, (0, 0), fx=0.1, fy=0.1)
    
    frame = cv2.medianBlur(frame,5)
    
            

    
    # colour detection limits
    lowerLimits = np.array([0, 0, 0])
    upperLimits = np.array([86, 167, 114])	
    thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
    opening = cv2.morphologyEx(thresholded, cv2.MORPH_OPEN, kernel)

    Robot = cv2.medianBlur(frame,5)
    Robot = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lowerLimits = np.array([0, 94, 69])
    upperLimits = np.array([146, 255, 217])
    thresholded1 = cv2.inRange(Robot, lowerLimits, upperLimits)
    Robot = cv2.bitwise_not(thresholded1)
    opening1 = cv2.morphologyEx(thresholded1, cv2.MORPH_OPEN, kernel)
    
    #Getting x and y coordinates of white pixels (255), putting them into a new list in order to use for the A* algorithm matplotlib syntax
    
    
    
    for i in range(len(opening)):
    ##    #print(i)
        for j in range(len(opening[i])):
            if opening[i][j] == 255:
                #print(i, j, opening[i][j])
                newlist = []
                newlist.append([i, j])
             #   print(newlist)
   
   
   # [[255, 0, 0]
    # [255, 255, 255]
    # [255, 0, 0]]
    # Our operations on the frame come here
    #linelocation = get_line_location(frame)
   

    
    #erosion = cv2.erode(thresholded,kernel,iterations = 1)
   
    
    #keypoints = detector.detect(erosion)
    keypoints = detector.detect(opening1)
    

   # start(robot)



    thresholded1 = cv2.drawKeypoints(thresholded1, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    for circle in keypoints:
        cv2.putText(frame, (str(int(circle.pt[0]))+ "  " +str(int(circle.pt[1]))), (int(circle.pt[0]),int(circle.pt[1])), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 2)

    

    cv2.imshow('tresh', opening)
    # cv2.imshow('Robot', thresholded1)
     # Display the resulting frame
   # cv2.imshow('Processed', outimage)   


    cv2.imshow('Robot', opening1)

    # Display the resulting frame
    cv2.imshow('Processed', Robot)
    
    cv2.imshow("Output", frame)    


    # Quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    
    t2=time.time()


# When everything done, release the capture
print('closing program')
cap.release()
cv2.destroyAllWindows()
